//
//  ViewController.h
//  webrtc
//
//  Created by luyao on 17-3-6.
//  Copyright (c) 2017年 reconova. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "webrtcclient.h"

@interface ViewController : UIViewController<UITextFieldDelegate, webrtcClientDelegate>
@property (strong, nonatomic) IBOutlet UITextField *peerNameTextField;
@property (strong, nonatomic) IBOutlet UITextField *signalServerAddress;
@property (weak, nonatomic) IBOutlet UIView *remoteVideo;

- (IBAction)callPeer:(id)sender;

@end
