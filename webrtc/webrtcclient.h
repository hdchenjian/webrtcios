#import <Foundation/Foundation.h>
#import "SocketRocket/SocketRocket.h"

#import "WebRTC/RTCEAGLVideoView.h"
#import "WebRTC/RTCMediaStream.h"
#import "WebRTC/RTCPeerConnectionFactory.h"
#import "WebRTC/RTCMediaConstraints.h"
#import "WebRTC/RTCPeerConnection.h"
#import "WebRTC/RTCVideoTrack.h"
#import "WebRTC/RTCAudioTrack.h"
#import "WebRTC/RTCICECandidate.h"
#import "WebRTC/RTCIceServer.h"
#import "WebRTC/RTCConfiguration.h"
#import "WebRTC/RTCSessionDescription.h"
#import "WebRTC/RTCDataChannel.h"
#import "WebRTC/RTCDataChannelConfiguration.h"

@interface PeerConnectionParameters : NSObject
@property (nonatomic) BOOL videoCodecHwAcceleration;
@property (nonatomic) NSInteger videoFps;
@end

@protocol webrtcClientDelegate <NSObject>
- (void)onStatusChanged:(NSInteger)newStatus;
- (void) onAddRemoteStream:(RTCVideoTrack* )remoteStream;
- (void) onRemoveRemoteStream;
- (void) onPeerLeave;
@end

@interface webrtcClient : NSObject
- (instancetype)initWithDelegate:(id<webrtcClientDelegate>)delegate;
- (void)initWebsocket:(NSString* )socketAddress hostIP: (NSString* )hostIP peerName: peerName;
- (BOOL)hasCallInit;
- (BOOL)isConnected;
- (void)uninitWebsocket;
- (void)startVideo:(PeerConnectionParameters* )params;
- (void)stopVideo;
- (void)sendMessage:(NSString* )message;
- (void)sendMessage:(NSString* )type payload:(NSMutableDictionary* )payload;
- (NSInteger)requestGetFile:(NSString* )fileName savePath:(NSString* ) savePath;
- (void)cancelFile;
@end
