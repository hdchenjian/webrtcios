//
//  ViewController.m
//  webrtc
//
//  Created by luyao on 17-3-6.
//  Copyright (c) 2017年 reconova. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>

#import "ViewController.h"
//#import "ARDVideoCallView.h"

@interface ViewController () <RTCEAGLVideoViewDelegate>
@property(nonatomic, readonly) RTCEAGLVideoView *remoteVideoView;
@property(nonatomic, strong) RTCVideoTrack *remoteVideoTrack;
@end

@implementation ViewController {
    webrtcClient* client;
    CGSize _remoteVideoSize;
}
@synthesize remoteVideoView = _remoteVideoView;
@synthesize remoteVideoTrack = _remoteVideoTrack;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self.peerNameTextField setDelegate:self];
    [self.signalServerAddress setDelegate:self];
    self.signalServerAddress.tag = 0;
    self.peerNameTextField.tag = 1;

    client = [[webrtcClient alloc] initWithDelegate:self];
    //[self.remoteView setDelegate:self];
    _remoteVideoView = [[RTCEAGLVideoView alloc] initWithFrame:self.remoteVideo.bounds];
    _remoteVideoView.delegate = self;
    _remoteVideoView.transform = CGAffineTransformMakeScale(-1, 1);
    [self.remoteVideo addSubview:_remoteVideoView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.signalServerAddress becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSLog(@"You entered %@",self.peerNameTextField.text);
    if(textField.tag == 0) {
        [self.signalServerAddress resignFirstResponder];
    } else {
        [self.peerNameTextField resignFirstResponder];
    }
    return YES;
}

- (IBAction)callPeer:(id)sender {
    NSString *peerName = self.peerNameTextField.text;
    NSString *signalServerAddress = self.signalServerAddress.text;
    NSLog(@"connect to %@ : %@", signalServerAddress, peerName);
    signalServerAddress = @"p2p0.reconova.com:3000";
    NSArray* host_port = [signalServerAddress componentsSeparatedByString:@":"];
    NSString* ip;
    NSString* port;
    if([host_port count] == 2) {
        ip = host_port[0];
        port = host_port[1];
    } else {
        NSLog(@"server address format error");
        return;
    }
    signalServerAddress = [NSString stringWithFormat:@"%@%@%@%@%@", @"ws://", ip, @":", port, @"/"];
    NSLog(@"connect to %@ : %@", signalServerAddress, peerName);

    [client initWebsocket:signalServerAddress hostIP:ip peerName:peerName];
    PeerConnectionParameters* params;
    params.videoCodecHwAcceleration = YES;
    params.videoFps = 15;
    [client startVideo:params];
}

// implement webrtcClientDelegate
- (void)onStatusChanged:(NSInteger)newStatus {}
- (void) onAddRemoteStream:(RTCVideoTrack* )track {
    _remoteVideoTrack = track;
    [_remoteVideoTrack addRenderer:_remoteVideoView];
}
- (void) onRemoveRemoteStream {}
- (void) onPeerLeave {
    NSLog(@"Peer Leave");
}

// RTCEAGLVideoViewDelegate
- (void)videoView:(RTCEAGLVideoView *)videoView didChangeVideoSize:(CGSize)size {
    if (videoView == _remoteVideoView) {
    _remoteVideoSize = size;
  }
  [self updateVideoViewLayout];
}

- (void)updateVideoViewLayout {
  CGSize defaultAspectRatio = CGSizeMake(4, 3);
  CGSize remoteAspectRatio = CGSizeEqualToSize(_remoteVideoSize, CGSizeZero) ?
      defaultAspectRatio : _remoteVideoSize;

  CGRect remoteVideoFrame =
      AVMakeRectWithAspectRatioInsideRect(remoteAspectRatio, self.remoteVideo.bounds);
  self.remoteVideoView.frame = remoteVideoFrame;
}
@end
