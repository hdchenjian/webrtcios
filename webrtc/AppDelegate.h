//
//  AppDelegate.h
//  webrtc
//
//  Created by luyao on 17-3-6.
//  Copyright (c) 2017年 reconova. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
